# Moje Repozytorium




# Paula Czajkowska

## O mnie
Cześć, nazywam się Paula Czajkowska, jestem początkującym testerem oprogramowania, w tym miejscu chciałbym podzielić się z Tobą moim doświadczeniem oraz projektami, które miałem dotychczas przyjemność wykonać. 
<center>

![profile](img/profile.jpg)

</center>

## Testowanie
Testowanie oprogramowania jest niezmiennie ważnym elementem procesu wytwarzania oprogramowania. Moje pierwsze kroki w tym kierunku stawiałem dość nieświadomie korzystając
z różnych aplikacji, w których miałem okazje znaleźć pewne nieścisłości. Jestem osobą, która lubi poszukać dziury w całym, a jednocześnie skupiam się na detalach. Postanowiłem, że zostanę testerem oprogramowania i moje umiejętności przydadzą się w Twoim zespole.

## Kurs Software Development Academy
Miałam przyjemność uczestniczyć w kursie "Tester oprogramowania" organizowanym przez Software Development Academy. Przez 105 godzin zajęć oraz wiele godzin poświęconych na pracę samodzielną zdobyłam wiedzę z następujących tematów:
<center>

[Certyfikat szkolenia](https://app.diplomasafe.com/pl-PL/diploma/d5113e3c90933e5bcff2bbb27e50a49617e654521)

</center>

Sprawnie będę poruszała się także w projektach zwinnych, dzięki zajęciom wprowadzających do metodyki Scrum:

<center>

![profile](img/scrum_certificate.png)

</center>

## Git oraz HTTP
W czasie kursu nauczyłam się nie tylko testować, ale rozwijałam swoje umiejętności w wielu kierunkach między innymi:

* Nauczyłam się pracy z Narzędziem GIT (oraz Gitlab)

* Nauczyłam się podstaw REST API oraz narzędzi sieciowych, dzięki czemu w przyszłej pracy, będę mógła testować back-end przy pomocy odpowiednich narzędzi.

## Certyfikat ISTQB
W czasie kursu przygotowywaliśmy się do egzaminu ISTQB Foundation Level, do którego obecnie się przygotowuję i planuję w najbliższym czasie podejść.



## Zadania, które wykonywałem w czasie kursu:
<center>

* Wprowadzenie do testowania | Cykl życia oprogramowania, Podstawy testowania, typy testów, Proces testowy, Podstawy techniczne
* Techniki projektowania testów | Techniki czarnoskrzynkowe, baiłoskrzynkowe, praca z dokumentacją testową
* Testowanie w oparciu o ryzyko | Identyfikacja i analiza ryzyka, ryzyko projektowe i produktowe, zarządzanie ryzykiem.
* Testowanie wspierane narzędziami | Repozytoria wymagań testów i incydentów, Narzędzia wspomagające debugowanie oraz raportowanie testów. Zajęcia praktyczne.
* Bazy danych | Model organizacji bazy danych, komunikacja między użytkownikiem lub aplikacją o relacyjną bazą danych za pomocą języka SQL.
* Automatyzacja testów | Wprowadzenie do programowania, Podstawy Seleniuum WebDriver. Testowanie w metodyce BDD.
* Projekt podsumowujący | Projekt do przetestowania polegający na zastosowaniu zdobytej wiedzy w praktyce.
* Przygotowanie do ISTQB Level Foundation | Podstawy testowania, Testowanie w cyklu życia oprogramowania, Statyczne techniki testowania, Techniki projektowania testów, zarządzanie testowaniem, narzędzia wspierające testowanie.

</center>

## Moje projekty

* [Przypadki Testowe](Przypadki Testowe)  dla strony [Connectors Poland](https://connectors.pl/)

* [Przypadki Testowe](Przypadki Testowe) dla strony [Electro](https://www.electro.pl/lp,k-2651-nawet-400-zl-taniej?clickId=146538)

* [Zapytania](Zapytania) REST API w ramach ćwiczeń.

* Testy Automatyczne pisane w programie InteliJ.

* Testy Automatyczne z wykorzystaniem Selenium WebDriver.

## Technologie

<center>

![profile](img/technical_skills.png)

</center>

## Zainteresowania
Testowanie to nie wszystko, w wolnym czasie jestem zaciętym graczem w grach typu Moba, MMO RPG i Single Player.

<center>

![mtb](img/mtb.jpg)

</center>

## Kontakt

Skontaktuj się ze mną mailowo: paula.czajkowska.p@gmail.com

Linkedin: [Paula Czajkowska](https://www.linkedin.com/in/paula-czajkowska-a4709b116/)
